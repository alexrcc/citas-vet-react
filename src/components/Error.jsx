const Error = ({mensaje}) =>{
    return(
        <div>
            <p className='font-bold text-white text-center uppercase mb-3 bg-red-500 rounded-md'>
                {mensaje}
            </p>
        </div>
    )
}
export default Error