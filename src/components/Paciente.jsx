function Paciente({paciente, setPaciente, eliminarPaciente}) {
    const{nombre, propietario, email, fecha, sintomas,id} = paciente
    const handleEliminar = ()=>{
        const eliminar = confirm('¿Esta seguro que desea Eliminar?')
        if(eliminar){
            eliminarPaciente(id)
        }
    }
    return (
        <div className="mx-5 mb-3 bg-white shadow-md px-5 py-10 rounded-xl">
        <p className="font-bold uppercase mb-3 text-gray-700">
            Nombre: {''}
            <span className="font-normal normal-case">{nombre}</span>
        </p>
        <p className="font-bold uppercase mb-3 text-gray-700">
            Propietario: {''}
            <span className="font-normal normal-case">{propietario}</span>
        </p>
        <p className="font-bold uppercase mb-3 text-gray-700">
            Email: {''}
            <span className="font-normal normal-case">{email}</span>
        </p>
        <p className="font-bold uppercase mb-3 text-gray-700">
            Fecha alta: {''}
            <span className="font-normal normal-case">{fecha}</span>
        </p>
        <p className="font-bold uppercase mb-3 text-gray-700">
            Sintomas: {''}
            <span className="font-normal normal-case">{sintomas}</span>
        </p>
        <div className="flex">
            <button
            className="py-2 px-10 mt-5 mr-5 bg-indigo-600 hover:bg-indigo-800 rounded-md text-white uppercase font-bold"
                type="button"
                onClick={()=>setPaciente(paciente)}
            >Editar</button>
            <button
            className="py-2 px-10 mt-5 bg-red-600 hover:bg-red-800 rounded-md text-white uppercase font-bold"
            type="button" onClick={handleEliminar}>
            Eliminar
            </button>
        </div>
    </div>
    )
  }
  
  export default Paciente