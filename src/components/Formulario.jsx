import {useState, useEffect} from 'react'
import Error from './Error'

function Formulario({pacientes,setPacientes,paciente,setPaciente}){
    //Hook useState
    const[nombre, setNombre] =useState("")
    const[propietario, setPropietario] =useState("")
    const[email, setEmail] =useState("")
    const[fecha, setFecha] =useState("")
    const[sintomas, setSintomas] =useState("")
    const[error, setError]= useState(false)
    
    useEffect(()=>{
        if(Object.keys(paciente).length>0){
            setNombre(paciente.nombre)
            setPropietario(paciente.propietario)
            setEmail(paciente.email)
            setFecha(paciente.fecha)
            setSintomas(paciente.sintomas)
        }
    },[paciente])

    const handleSubmit = (e) => {
        e.preventDefault()
        if([nombre, propietario, email, fecha, sintomas].includes('')){
            setError(true);
            return;
        }
    const generarId = () =>{
        const random = Math.random().toString().substring(2);
        const fecha = Date.now().toString(36)
        return random + fecha;
    }
        setError(false)
        //Objeto de paciente
        const objetoPaciente={
            nombre,
            propietario,
            email,
            fecha,
            sintomas,
            //id: generarId()
        }
        if(paciente.id){ // Si ya existe el ID es por que estoy actualizando
            objetoPaciente.id = paciente.id // El id que debe tener es igual al id que tiene; es decir no genero uno nuevo
            const pacientesActualizados = pacientes.map( // Genero un arreglo de pacientes y recorro los pacientes
                pacienteState =>pacienteState.id === paciente.id ? objetoPaciente : pacienteState // cuando encuentre un id
                //igual entonces retorna o le agrega el Objeto actualizado y cuando son diferentes agrega los pacientes no acutlaiados
            );
            setPacientes(pacientesActualizados) // uso el estate para setear la nueva lista de pacientes
            setPaciente({})
        }else{
            objetoPaciente.id= generarId()
            setPacientes([...pacientes, objetoPaciente]) // uso el estate para agregar el objeto paciente al arreglo ede pacientes

        }
        
        //Reiniciar el form
        setNombre('')
        setEmail('')
        setPropietario('')
        setSintomas('')
        setFecha('')
        
    }       
    return(
    <div className="md:w-1/2 lg:w-2/5 mx-5">
            <h2 className="font-black text-3xl text-center">Seguimiento Pacientes</h2>
            <p className="text-lg mt-5 text-center mb-10">Añade Pacientes y {''} 
            <span className="font-bold text-indigo-600">Administralos</span></p>
        <form className="bg-white shadow-md rounded-lg py-10 px-5 mb-10" onSubmit={handleSubmit}>
            {error && (<Error mensaje={'*Todos los campos son requeridos'}/>)}
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="mascota">
                    Nombre Mascota
                </label>
                <input id="mascota" className="border-2 w-full p-1 mt-2 placeholder-gray-400 rounded-md"
                type="text"
                value={nombre}
                onChange={(e)=>setNombre(e.target.value)}
                placeholder="Nombre de la mascota"
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="propietario">
                    Nombre Propietario
                </label>
                <input id="propietario" className="border-2 w-full p-1 mt-2 placeholder-gray-400 rounded-md"
                type="text"
                value={propietario}
                onChange={(e)=>setPropietario(e.target.value)}
                placeholder="Nombre del propietario"
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="email">
                    Email
                </label>
                <input id="email" className="border-2 w-full p-1 mt-2 placeholder-gray-400 rounded-md"
                type="email"
                value={email}
                onChange={(e)=>setEmail(e.target.value)}
                placeholder="Email contacto propieario"
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="alta">
                    Alta
                </label>
                <input id="alta" className="border-2 w-full p-1 mt-2 placeholder-gray-400 rounded-md"
                type="date"
                value={fecha}
                onChange={(e)=>setFecha(e.target.value)}
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="sintomas">
                    Síntomas
                </label>
               <textarea id="sintomas" className="border-2 w-full p-1 mt-2 placeholder-gray-400 rounded-md"
               placeholder="describe los síntomas"
               value={sintomas}
                onChange={(e)=>setSintomas(e.target.value)}/>
            </div>
                <input type="submit" 
                className="bg-indigo-600 w-full p-3 text-white uppercase font-bold hover:bg-indigo-700 cursor-pointer transition-all"
                value={paciente.id ? 'Editar Paciente': 'Agregar paciente'}/>        
        </form>
    </div>
    )
}
export default Formulario