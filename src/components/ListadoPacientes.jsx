import Paciente from "./Paciente"
import { useEffect } from "react"

function ListadoPacientes({pacientes, setPaciente,eliminarPaciente}){
    useEffect(()=>{
        if(pacientes.length> 0)
            console.log('Nuevo paciente agregado')
    },[pacientes])
    return(
        <div className="md:w-1/2 lg:w-3/5 ">
            
            <h2 className="font-black text-3xl text-center">{pacientes && pacientes.length ? "Listado Pacientes":"No ha registrado pacientes"}</h2>
            <p className="text-xl mt-5 mb-10 text-center">Administra tus {''}<span className="text-indigo-600 font-bold">Pacientes y Citas</span></p>
            <div className="md:h-screen md:overflow-y-scroll">
            {pacientes.map((paciente)=>(
                <Paciente 
                    key={paciente.id}
                    paciente={paciente}
                    setPaciente= {setPaciente}
                    eliminarPaciente={eliminarPaciente}
                />

            ))} 
            </div>
        </div>
    )
}
export default ListadoPacientes